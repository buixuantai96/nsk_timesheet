package SeleniumWD;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.FocusTraversalPolicy;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.DefaultCaret;

public class Open2TestRunner {
	private JFrame frmOpen2Test;	
	private JTextField textTestSuite;
	private ButtonGroup btnGroup;
	private JTextArea textAreaConsole;
	private JCheckBox checkboxUseRemoteWebdriver;
	private JTextField tbRemoteWDAddress;
	private JComboBox<Object> cbBrowser;
	private JTextField textDataFilePath;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Open2TestRunner window = new Open2TestRunner();
					window.frmOpen2Test.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Open2TestRunner() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmOpen2Test = new JFrame();
		frmOpen2Test.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				Preferences prefs = Preferences.userRoot().node(this.getClass().getName());
				String t = prefs.get(PREFS.PREF_BROWSER_TYPE, "FF");
				
				Enumeration<AbstractButton> btns =	btnGroup.getElements();
				while(btns.hasMoreElements()) {
					AbstractButton btn = btns.nextElement();
					if(btn.getActionCommand().equalsIgnoreCase(t)) {
						btnGroup.setSelected(btn.getModel(), true);
					} else {
						btnGroup.setSelected(btn.getModel(), false);
					}
			    }
				
				textTestSuite.setText(prefs.get(PREFS.PREF_SELENIUM_UTILITY_FILE, ""));
				
				checkboxUseRemoteWebdriver.setSelected(prefs.getBoolean(PREFS.PREF_USE_REMOTE, false));
				
				tbRemoteWDAddress.setEnabled(checkboxUseRemoteWebdriver.isSelected());
				tbRemoteWDAddress.setText(prefs.get(PREFS.PREF_REMOTE_ADDRESS, ""));
				//textField.setEnabled(chckbxNewCheckBox.isSelected());
			}
			@Override
			public void windowClosing(WindowEvent arg0) {
				Preferences prefs = Preferences.userRoot().node(this.getClass().getName());				
				prefs.put(PREFS.PREF_BROWSER_TYPE, btnGroup.getSelection().getActionCommand());
				prefs.put(PREFS.PREF_SELENIUM_UTILITY_FILE, textTestSuite.getText());
				
				prefs.putBoolean(PREFS.PREF_USE_REMOTE, checkboxUseRemoteWebdriver.isSelected());		
				prefs.put(PREFS.PREF_REMOTE_ADDRESS, tbRemoteWDAddress.getText());

			}
		});
		frmOpen2Test.setResizable(false);
		frmOpen2Test.setTitle("NSK Team");
		frmOpen2Test.setBounds(100, 100, 1050, 591);
		frmOpen2Test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmOpen2Test.getContentPane().setLayout(null);
		
		JLabel lblSeleniumutilityFile = new JLabel("Log work data:");
		lblSeleniumutilityFile.setBounds(31, 25, 120, 14);
		frmOpen2Test.getContentPane().add(lblSeleniumutilityFile);
		
		JPanel panel = new JPanel();
		panel.setVisible(false);
		panel.setBorder(new LineBorder(Color.GRAY, 2, true));
		panel.setBounds(586, 552, 317, 62);
		frmOpen2Test.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Browser:");
		lblNewLabel.setBounds(10, 10, 59, 14);
		panel.add(lblNewLabel);
		
		btnGroup = new ButtonGroup();

		JRadioButton rdbtnIE = new JRadioButton("IE");
		rdbtnIE.setSelected(true);
		rdbtnIE.setActionCommand("IE");
		rdbtnIE.setToolTipText("Mircosoft Internet Explorer");
		rdbtnIE.setBounds(64, 6, 46, 23);		
		panel.add(rdbtnIE);		
		btnGroup.add(rdbtnIE);
		
		JRadioButton rdbtnFF = new JRadioButton("FF");
		rdbtnFF.setActionCommand("FF");
		rdbtnFF.setToolTipText("Mozilla Firefox");
		rdbtnFF.setBounds(112, 6, 46, 23);
		rdbtnFF.setSelected(true);
		panel.add(rdbtnFF);
		btnGroup.add(rdbtnFF);
		
		JRadioButton rdbtnGChrome = new JRadioButton("Chrome");
		rdbtnGChrome.setToolTipText("Google Chrome");
		rdbtnGChrome.setSelected(true);
		rdbtnGChrome.setActionCommand("GC");
		rdbtnGChrome.setBounds(160, 6, 78, 23);
		panel.add(rdbtnGChrome);
		btnGroup.add(rdbtnGChrome);
		
		JRadioButton rdbtnOpera = new JRadioButton("Opera");
		rdbtnOpera.setEnabled(false);
		rdbtnOpera.setToolTipText("Opera browser");
		rdbtnOpera.setSelected(true);
		rdbtnOpera.setActionCommand("OP");
		rdbtnOpera.setBounds(240, 6, 64, 23);
		panel.add(rdbtnOpera);
		btnGroup.add(rdbtnOpera);
		
		checkboxUseRemoteWebdriver = new JCheckBox("Remote:");
		checkboxUseRemoteWebdriver.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				tbRemoteWDAddress.setEnabled(checkboxUseRemoteWebdriver.isSelected());
			}
		});
		
		checkboxUseRemoteWebdriver.setBounds(10, 31, 84, 23);
		panel.add(checkboxUseRemoteWebdriver);
		
		//chckbxUseRemoteWebdriver.setVisible(false);
		
		tbRemoteWDAddress = new JTextField();
		tbRemoteWDAddress.setEnabled(false);
		tbRemoteWDAddress.setBounds(95, 31, 200, 20);
		panel.add(tbRemoteWDAddress);
		tbRemoteWDAddress.setColumns(10);
		//tbRemoteWDAddress.setVisible(false);
		
		JButton btnRun = new JButton("Run");
		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//JOptionPane.showMessageDialog(null, cbBrowser.getSelectedItem());	
				//return;
				final File f = new File(textTestSuite.getText());				
				if(!f.exists()) {
					System.out.println("Log work data file doesn't exit!");
					JOptionPane.showMessageDialog(null, "Log work data file doesn't exit!");
					return;
				}
				try {
			        Thread t = new Thread(new Runnable() {
			            public void run() {	
			            	PrintStream stdout = System.out;
							PrintStream stderr = System.err;
							
							TextAreaOutputStream out = new TextAreaOutputStream (textAreaConsole);
					        System.setOut (new PrintStream (out));
					        System.setErr(new PrintStream (out));
					        DefaultCaret caret = (DefaultCaret)textAreaConsole.getCaret();
					        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
					        
			            	System.out.println("start");
			            	SeleniumWD swd = new SeleniumWD();
							try {
								swd.SetDataFilePath("ABC");
								swd.readUtilFile();
							} catch (Exception e) {
								e.printStackTrace();
							}	
							
							try {
								//swd.close();
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							System.out.println("end");
							System.setOut(stdout);
							System.setErr(stderr);
			            }
			        });
			        t.start();
			        
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		
		//btnRun.setBounds(312, 61, 89, 23);
		btnRun.setBounds(924, 19, 100, 23);
		frmOpen2Test.getContentPane().add(btnRun);
		
		textTestSuite = new JTextField();
		textTestSuite.setBounds(135, 22, 670, 20);
		frmOpen2Test.getContentPane().add(textTestSuite);
		textTestSuite.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(26, 60, 1000, 450);
		frmOpen2Test.getContentPane().add(scrollPane);
		
		textAreaConsole = new JTextArea();
		scrollPane.setViewportView(textAreaConsole);
		textAreaConsole.setEditable(false);
		JButton btnClearLog = new JButton("Clear log");
		btnClearLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textAreaConsole.setText("");
			}
		});
		
		btnClearLog.setBounds(400, 525, 89, 23);
		frmOpen2Test.getContentPane().add(btnClearLog);
		
		JButton btnSaveLog = new JButton("Save log");
		btnSaveLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser c = new JFileChooser();
				c.setFileFilter(new FileNameExtensionFilter("Log file", "log"));
				c.setSelectedFile(new File("test.log"));
			    int rVal = c.showSaveDialog(frmOpen2Test);
			    if (rVal == JFileChooser.APPROVE_OPTION) {
			    	JOptionPane.showMessageDialog(frmOpen2Test, c.getSelectedFile().getAbsolutePath());
			    	try {
			            File file = new File(c.getSelectedFile().getAbsolutePath());
			            BufferedWriter output = new BufferedWriter(new FileWriter(file));
			            output.write(textAreaConsole.getText());
			            output.close();
			          } catch ( IOException e ) {
			             e.printStackTrace();
			          }
			    }
			}
		});
		btnSaveLog.setBounds(500, 525, 89, 23);
		frmOpen2Test.getContentPane().add(btnSaveLog);
		
		JButton btnBrowse = new JButton("Browse...");
		btnBrowse.setBounds(820, 19, 100, 23);
		frmOpen2Test.getContentPane().add(btnBrowse);
		
		JLabel lblBrowser = new JLabel("Browser:");
		lblBrowser.setBounds(31, 65, 120, 14);
		//frmOpentest.getContentPane().add(lblBrowser);
		
		cbBrowser = new JComboBox<Object>();
		cbBrowser.setModel(new DefaultComboBoxModel<Object>(new String[] {"Mozilla Firefox", "Internet Explorer"}));
		cbBrowser.setSelectedIndex(0);
		cbBrowser.setBounds(156, 62, 146, 20);
		//frmOpentest.getContentPane().add(cbBrowser);
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser chooser = new JFileChooser();
			    FileNameExtensionFilter filter = new FileNameExtensionFilter("MS Excell", "xls", "xlsx");
			    chooser.setFileFilter(filter);
			    int returnVal = chooser.showOpenDialog(null);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			    	textDataFilePath.setText(chooser.getSelectedFile().getAbsolutePath());
			       System.out.println("You chose to open this file: " +
			            chooser.getSelectedFile().getName());
			    }
			}
		});
		
		Vector<Component> order = new Vector<Component>(5);
		order.add(textDataFilePath);
		order.add(btnBrowse);
		order.add(btnRun);
		//order.add(textAreaConsole);
		order.add(btnClearLog);
		order.add(btnSaveLog);
		frmOpen2Test.setFocusable(true);
		frmOpen2Test.setFocusTraversalPolicy(new MyOwnFocusTraversalPolicy(order));
	}
}

class MyOwnFocusTraversalPolicy extends FocusTraversalPolicy {
	Vector<Component> order;
	public MyOwnFocusTraversalPolicy(Vector<Component> order) {
		this.order = new Vector<Component>(order.size());
		this.order.addAll(order);
	}
	public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
		int idx = (order.indexOf(aComponent) + 1) % order.size();
		return order.get(idx);
	}
	public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {
		int idx = order.indexOf(aComponent) - 1;
		if (idx < 0) {
            idx = order.size() - 1;
        }
		return order.get(idx);
	}
	
	public Component getDefaultComponent(Container focusCycleRoot) {
        return order.get(0);
    }
	
	public Component getLastComponent(Container focusCycleRoot) {
        return order.lastElement();
    }
	
	public Component getFirstComponent(Container focusCycleRoot) {
        return order.get(0);
    }
	
}