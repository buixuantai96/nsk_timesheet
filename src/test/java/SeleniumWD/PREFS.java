package SeleniumWD;

public class PREFS {
	public static final String PREF_SELENIUM_UTILITY_FILE = "PREF_SELENIUM_UTILITY_FILE";
	public static final String PREF_BROWSER_TYPE = "PREF_BROWSER_TYPE";
	public static final String PREF_USE_REMOTE = "PREF_USE_REMOTE";
	public static final String PREF_REMOTE_ADDRESS = "PREF_REMOTE_ADDRESS";
}
